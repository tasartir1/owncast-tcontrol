''' Owncast tControl
Implementation of Owncast API on https://owncast.online/api/latest/
compatible with Owncast 0.0.11

Additional docs:
https://requests.readthedocs.io
'''

import requests
from requests.structures import CaseInsensitiveDict
import sys
import base64
import json
import logging

#Read connection config file
with open('conf/connection.json', 'r', encoding='utf8') as conf_connection:
    connection = json.load(conf_connection)

with open('conf/config.json', 'r', encoding='utf8') as conf_config:
    config = json.load(conf_config)


#logging configuration
def log_level(level='notset'):
    level = level.lower()
    levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'notset': logging.NOTSET,
    }

    if level in levels:
        return levels[level]
    return levels[notset]

logging_level = log_level(config['logging']['level'])
logging_format = config['logging']['format']
logging_file = config['logging']['file']

logging.basicConfig(
    level = logging_level,
    format = logging_format,
    handlers=[
        logging.FileHandler(logging_file),
        logging.StreamHandler(sys.stdout)
    ]
)

# Authentication
# https://owncast.online/api/latest/#section/Authentication

## AdminBasicAuth
## The username for admin basic auth is admin and the password is the stream key.
AdminBasicAuth = CaseInsensitiveDict()
AdminBasicAuth['Content-Type'] = 'application/json'
AdminBasicAuth['Authorization'] = 'basic ' + base64.b64encode(('admin:{}'.format(connection['stream_key'])).encode()).decode()

## AccessToken
## 3rd party integration auth where a service user must provide an access token.
AccessToken = CaseInsensitiveDict()
AccessToken['Content-Type'] = 'application/json'
AccessToken['Authorization'] = f'Bearer {connection["access_token"]}'

## UserToken
## A standard user must provide a valid access token.
UserToken = CaseInsensitiveDict()
UserToken['Content-Type'] = 'application/json'
UserToken['Authorization'] = f'accessToken {connection["user_token"]}'

## ModeratorUserToken
## A moderator user must provide a valid access token.
ModeratorUserToken = CaseInsensitiveDict()
ModeratorUserToken['Content-Type'] = 'application/json'
ModeratorUserToken['Authorization'] = f'accessToken {connection["moderator_user_token"]}'


# Owncast API abstraction
def owncast_api(request, api, auth=None, body={}, server_url=connection['server_url']):
    ''' Abstraction of owncast api requests
    request (str): 'get' or 'post'
    auth (str): 'admin', 'integrations', None, #TODO: 'user', 'moderator'
    api (str): uri to api function
    '''
    api_url = server_url + api
    headers_auth = {
        'admin': AdminBasicAuth,
        'integrations': AccessToken
    }
    headers = None
    if auth in headers_auth:
        headers = headers_auth[auth]

    if request == 'get':
        r = requests.get(api_url, headers=headers)

    if request == 'post':
        data = ''
        if body: data = json.dumps(body).encode('utf-8')
        r = requests.post(api_url, headers=headers[auth], data=data)
    
    if r:
        return r


# Admin functions
# Admin operations requiring authentication.
def get_admin_status():
    '''Server status and broadcaster
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1status/get
    '''
    return owncast_api('get', '/api/admin/status', 'admin',)

def disconnect_broadcaster():
    '''Disconnect Broadcaster
    Disconnect the active inbound stream, if one exists, and terminate the broadcast.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1disconnect/post
    '''
    return owncast_api('post', '/api/admin/disconnect', 'admin')

def reset_yp_key():
    '''Reset your YP registration key.
    Used when there is a problem with your registration to the Owncast Directory
    via the YP APIs. This will reset your local registration key.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1yp~1reset/post
    '''
    return owncast_api('post', '/api/admin/yp/reset', 'admin')

def get_chat_clients():
    '''Return a list of currently connected clients
    Return a list of currently connected clients with optional geo details.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1chat~1clients/get
    '''
    return owncast_api('get', '/api/admin/chat/clients', 'admin',)

def get_disabled_users():
    '''Return a list of currently connected clients
    Return a list of currently connected clients with optional geo details.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1users~1disabled/get
    '''
    return owncast_api('get', '/api/admin/users/disabled', 'admin')

def get_logs():
    '''Return recent log entries
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1logs/get
    '''
    return owncast_api('get', '/api/admin/logs', 'admin')

def get_warnings():
    '''Return recent warning and error logs.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1logs~1warnings/get
    '''
    return owncast_api('get', '/api/admin/logs/warnings', 'admin')

def get_serverconfig():
    '''Server Configuration
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1serverconfig/get
    '''
    return owncast_api('get', '/api/admin/serverconfig', 'admin')

def get_chat_messages():
    '''Chat messages, unfiltered.
    Get a list of all chat messages with no filters applied.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1chat~1messages/get
    '''
    return owncast_api('get', '/api/chat/messages', 'admin')

def update_message_visibility(): #TODO
    #Update the visibility of chat messages.
    #/api/admin/chat/updatemessagevisibility
    #https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1chat~1updatemessagevisibility/post
    pass

def user_enable(): #TODO
    #Enable or disable a single user.
    #Enable or disable a single user. Disabling will also hide all the user's chat messages.
    #/api/admin/chat/users/setenabled
    #https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1chat~1users~1setenabled/post
    pass

def set_stream_key(value):
    '''Set the stream key.
    Set the stream key. Also used as the admin password.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1key/post
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/key', 'admin', body)

def set_stream_title(value):
    '''Set the stream title.
    Set the title of the currently streaming content.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1streamtitle/post
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/streamtitle', 'admin', body)

def set_server_name(value):
    '''Set the server name.
    Set the name associated with your server. Often is your name, username or identity.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1name/post
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/name', 'admin', body)

def set_server_summary(value):
    '''Set the server summary.
    Set the name associated with your server. Often is your name, username or identity.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1serversummary/post
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/serversummary', 'admin', body)

def set_page_content(value):
    '''Set the custom page content.
    Set the custom page content using markdown.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1pagecontent/post
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/pagecontent', 'admin', body)

def set_server_logo(value):
    '''Set the server logo.
    Set the logo for your server. Path is relative to webroot.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1logo/post
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/logo', 'admin', body)

def set_tags(value=''):
    '''Set the server tags.
    Set the tags displayed for your server and the categories you can show up in on the directory.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1tags/post
    Accepts list, str
    '''
    tags = []
    if type(value) == str:
        tags = [tag.strip() for tag in value.split(',')]
    if type(value) == list:
        tags = value

    body = {'value': tags}
    return owncast_api('post', '/api/admin/config/tags', 'admin', body)

def set_ffmpeg_path(value):
    '''Set the ffmpeg binary path
    Set the path for a specific copy of ffmpeg on your system.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1ffmpegpath/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/ffmpegpath', body)

def set_webserver_port(value):
    '''Set the owncast web port.
    Set the port the owncast web server should listen on.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1webserverport/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/webserverport', body)

def set_rtmp_port(value):
    '''Set the inbound rtmp server port.
    Set the port where owncast service will listen for inbound broadcasts.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1rtmpserverport/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/rtmpserverport', body)

def set_nsfw(value=False):
    '''Mark if your stream is not safe for work
    Mark if your stream can be consitered not safe for work. Used in different contexts,
    including the directory for filtering purposes.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1nsfw/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/nsfw', body)

def set_directoryenabled(value=False):
    '''Set if this server supports the Owncast directory.
    If set to true the server will attempt to register itself with the Owncast Directory. Off by default.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1directoryenabled/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/directoryenabled', body)

def set_serverurl(value):
    '''Set the public url of this owncast server.
    Set the public url of this owncast server. Used for the directory and optional integrations.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1serverurl/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/serverurl', body)

def set_latency(value):
    '''Set the latency level for the stream.
    Sets the latency level that determines how much video is buffered between the server and viewer.
    Less latency can end up with more buffering.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1video~1streamlatencylevel/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/video/streamlatencylevel', body)

def set_output_variants(value):
    '''Set the configuration of your stream output.
    Sets the detailed configuration for all of the stream variants you support.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1video~1streamoutputvariants/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/video/streamoutputvariants', body)

def set_video_codec(value='libx264'):
    '''Set the video codec.
    Sets the specific video codec that will be used for video encoding.
    Some codecs will support hardware acceleration. Not all codecs will be supported for all systems.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1video~1codec/post
    '''
    body = {'value': value}
    return owncast_api('post', 'admin', '/api/admin/config/video/codec', body)

def set_s3(enabled, endpoint, accessKey, secret, bucket, region):
    '''Set your storage configration.
    Sets your S3 storage provider configuration details to enable external storage.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1s3/post
    '''
    body = {
        'value': {
            'enabled': enabled,
            'endpoint': endpoint,
            'accessKey': accessKey,
            'secret': secret,
            'bucket': bucket,
            'region': region
        }
    }
    return owncast_api('post', 'admin', '/api/admin/config/s3', body)

def set_socialhandles(value):
    '''Set your social handles.
    Sets the external links to social networks and profiles.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1socialhandles/post
    accepts list of dict: [{'platform': 'github', 'url': 'http://example.com'}]
    '''
    body = {'value': value}
    return owncast_api('post', '/api/admin/config/socialhandles3', 'admin', body)

def suggested_user_names(value=''):
    '''A list of names to select from randomly for new chat users.
    https://owncast.online/api/latest/#tag/Admin/paths/~1api~1admin~1config~1chat~1suggestedusernames/post
    Accepts str (coma separated values)
    '''
    body = {'value': [value]}
    return owncast_api('post', '/api/admin/config/chat/suggestedusernames', 'admin', body)

# Chat functions

#Register a chat user
#/api/chat/register

#Chat Messages Backlog
#/api/chat

def get_custom_emoji():
    '''Get Custom Emoji
    '''
    return owncast_api('get', '/api/emoji', 'admin')

## Integrations functions
# APIs built to allow 3rd parties to interact with an Owncast server.

def send_chat_message(value):
    '''Send a chat message.
    '''
    body = {'body': value}
    return owncast_api('post', '/api/integrations/chat/send', 'integrations', body)

def send_system_chat_message(value):
    '''Send a system chat message.
    '''
    body = {'body': value}
    return owncast_api('post', '/api/integrations/chat/system', 'integrations', body)

def send_chat_action(value, author=""):
    '''Send a chat action.
    '''
    body = {'body': value}
    if author:
        body['author'] = author
    return owncast_api('post', '/api/integrations/chat/action', 'integrations', body)

#Send system chat message to a client, identified by its ClientId

#Create an access token.

#Delete an access token.

#return rll access tokens.

#Set external action URLs.

#return r list of currently connected clients

#Historical Chat Messages

#Update the visibility of chat messages.

## Server functions

def get_config():
    '''Information
    Returns dict: name, summary, logo, tags, version, nsfw, extraPageContent, streamTitle, socialHandles,
    chatDisabled, externalActions, customStyles, maxSocketPayloadSize, federation
    '''
    return owncast_api('get','/api/config')
    

def get_status():
    '''Current Status
    This endpoint is used to discover when a server is broadcasting, the number of active viewers as well as other useful information for updating the user interface.
    Returns dict: online, viewerCount, lastConnectTime, lastDisconnectTime, versionNumber, streamTitle
    '''
    return owncast_api('get','/api/status')

def get_yp():
    '''Yellow Pages Information
    Returns dict: name, description, logo, nsfw, tags, online, viewerCount, overallMaxViewerCount, sessionMaxViewerCount, lastConnectTime
    '''
    return owncast_api('get', '/api/integrations/chat/action')

# Derived functions

def get_stream_title():
    '''Returns stream title as str
    '''
    try:
        return get_config().json()['streamTitle']
    except (AttributeError, KeyError) as e:
        logging.error(f"get_stream_title: {e}")
        return ''

def get_tags(format='str'):
    '''Returns server tags as str (default), list
    '''
    try:
        tags = get_config().json()['tags']
    except (AttributeError, KeyError) as e:
        logging.error(f"get_tags: {e}")
        return ''

    if format == 'list':
        return tags

    if format == 'str':
        return ', '.join(tags)

def get_nsfw():
    '''Returns nsfw as bool
    '''
    try:
        return get_config().json()['nsfw']
    except (AttributeError, KeyError) as e:
        logging.error(f"get_nsfw: {e}")
        return ''
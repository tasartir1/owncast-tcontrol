# Owncast tControl

## Description
Goal of this project is make a control panel for some of the features of [Owncast self-hosted live video streaming server](https://owncast.online).

It mainly serves my needs and ideas how to control my stream. I call it 'a chatbot without a bot'. I personally don't like over utilisation of chatbots when I can achieve the same result without it.

## Screenshots
![Owncast tControl screenshot](img/owncast-screenshot.png)

## Installation
Prerequisites:
- Python 3
- tkinter

### Windows
Download and install the latest [Python 3](https://www.python.org/downloads/) or via `winget install Python.Python.3`.
During instalation check "install tcl/tk".

### Debian
Install packages `python3` and `python3-tk`.

## Config files
Copy conf/connection.json.example to conf/connection.json and fill in server_url and stream_key for admin functions. You can fill in user_token or access_token to use user and integration functions.

Copy conf/config.json.example to conf/config.json and fill in default text presets for GUI buttons and comboboxes.

## Usage
### GUI
Run `python gui.pyw`. On windows you may doubleclick gui.pyw directly.

### CLI
TODO

## Licence
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/tasartir1/owncast-tcontrol/">Owncast tControl</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://tasartir.cz">Ondřej Korba</a> is marked with <a href="http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1"></a></p>

## Roadmap
TODO:
- Clean up code with Owncast API implementation
- Sending messages and confirming values with Enter

## Project status
Early release.
Compatibile with Owncast API version 0.0.11.

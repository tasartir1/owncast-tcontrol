import owncast
try:
	import tkinter as tk
except Exception as e:
	print(str(e))
	sys.exit(1)
from tkinter import ttk

#Variables
stream_titles = owncast.config['stream_titles']
system_chat = owncast.config['system_chat']
chat_actions = owncast.config['chat_actions']
start_pause_text = owncast.config['start_pause_text']
end_pause_text = owncast.config['end_pause_text']
shoutouts = owncast.config['shoutouts']
suggested_names = owncast.config['suggested_names']
tags = owncast.config['tags']

#Main window
main = tk.Tk()
main.title('Owncast tControl')
main.iconbitmap('./img/icon.ico')

selected_title = tk.StringVar()
selected_system_message = tk.StringVar()
selected_chat_action = tk.StringVar()
selected_tags = tk.StringVar()

##Append current stream title to stream title list
current_stream_title=owncast.get_stream_title()
if current_stream_title not in stream_titles: stream_titles.append(current_stream_title)
##Append current tags to server tags list
current_tags=owncast.get_tags("str")
if current_tags not in tags: tags.append(current_tags)

#Stream title
streamtitle_frame = ttk.LabelFrame(
	main,
	text='Stream title'
)
streamtitle_frame.pack(fill='x')

streamtitle_combobox = ttk.Combobox(
	streamtitle_frame,
	textvariable=selected_title,
	width=50
)
streamtitle_combobox['values'] = stream_titles
streamtitle_combobox.set(current_stream_title)
streamtitle_combobox.pack(side='left')

streamtitle_button = ttk.Button(
	streamtitle_frame, text='Set title',
	command=lambda: owncast.set_stream_title(selected_title.get())
)
streamtitle_button.pack(fill='x')

#System chat
system_chat_frame = ttk.LabelFrame(
	main,
	text='Server messages'
)
system_chat_frame.pack(fill='x')

system_chat_combobox = ttk.Combobox(
	system_chat_frame,
	textvariable=selected_system_message,
	width=50
)

if len(system_chat) > 0:
	system_chat_combobox['values'] = system_chat
	system_chat_combobox.current(0)

system_chat_combobox.pack(side='left')

system_chat_button = ttk.Button(
	system_chat_frame,
	text='Send server message',
	command=lambda: owncast.send_system_chat_message(selected_system_message.get())
)
system_chat_button.pack(fill='x')

#Chat action message
chat_action_frame = ttk.LabelFrame(
	main,
	text='Chat action'
)
chat_action_frame.pack(fill='x')

chat_action_combobox = ttk.Combobox(
	chat_action_frame,
	textvariable=selected_chat_action,
	width=50
)

if len(chat_actions) > 0:
	chat_action_combobox['values'] = chat_actions
	chat_action_combobox.current(0)

chat_action_combobox.pack(side='left')

chat_action_button = ttk.Button(
	chat_action_frame, text='Send chat action',
	command=lambda: owncast.send_chat_action(selected_chat_action.get())
)
chat_action_button.pack(fill='x')

#Stream tags
tags_frame = ttk.LabelFrame(
	main,
	text='Stream tags'
)
tags_frame.pack(fill='x')

tags_combobox = ttk.Combobox(
	tags_frame,
	textvariable=selected_tags,
	width=50
)
tags_combobox['values'] = tags
tags_combobox.set(current_tags)
tags_combobox.pack(side='left')

tags_button = ttk.Button(
	tags_frame,
	text='Set tags',
	command=lambda: owncast.set_tags(selected_tags.get())
)
tags_button.pack(fill='x')

#Shoutouts
shoutouts_frame = ttk.LabelFrame(
	main,
	text='Shoutouts'
)
shoutouts_frame.pack(side='left', fill='y')

for so, so_message in shoutouts.items():
	shoutout_button = ttk.Button(
		shoutouts_frame,
		text=so,
		command=lambda message = so_message: owncast.send_system_chat_message(message)
	)
	shoutout_button.pack(fill='x')

##Pause message
pause_frame = ttk.LabelFrame(
	main,
	text='Pause message'
)
pause_frame.pack(side='left', fill='both')

pausestart_button = ttk.Button(
	pause_frame,
	text='Start pause',
	command=lambda: owncast.send_chat_action(start_pause_text)
)
pausestart_button.pack(fill='x')

pauseend_button = ttk.Button(
	pause_frame,
	text='End pause',
	command=lambda: owncast.send_chat_action(end_pause_text)
)
pauseend_button.pack(fill='x')

#Settings
settings_frame = ttk.LabelFrame(
	main,
	text='Settings'
)
settings_frame.pack(fill='y', side='left')

suggested_names_button = ttk.Button(
	settings_frame,
	text='Upload User Names',
	command=lambda: owncast.suggested_user_names(suggested_names)
)
suggested_names_button.pack(fill='x')

nsfwon_button = ttk.Button(
	settings_frame,
	text='NSFW On',
	command=lambda: owncast.set_nsfw(True)
)
nsfwon_button.pack(fill='x')

nsfwoff_button = ttk.Button(
	settings_frame,
	text='NSFW Off',
	command=lambda: owncast.set_nsfw(False)
)
nsfwoff_button.pack(fill='x')

main.mainloop()
